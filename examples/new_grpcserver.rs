use sdr_grpc_lib::server::GrpcServer;
use std::error::Error;
use tokio::runtime::Runtime;

async fn spawn_server(addr: String) -> Result<(), Box<dyn Error>> {
    println!("spawning new server");
    GrpcServer::default()
        .set_service_address(addr)
        .spawn_with_default_service()
        .await
}

fn spawn_servers() -> Result<(), Box<dyn std::error::Error>> {
    let t_handle = std::thread::spawn(move || {
        let addr = "[::1]:50051";
        let rt = Runtime::new().unwrap();
        rt.block_on(async { spawn_server(addr.to_string()).await.unwrap() });
    });

    let x_handle = std::thread::spawn(move || {
        let addr = "[::1]:50052";
        let rt = Runtime::new().unwrap();
        rt.block_on(async { spawn_server(addr.to_string()).await.unwrap() });
    });

    t_handle.join().unwrap();
    x_handle.join().unwrap();
    /*
    std::thread::spawn(move || -> Result<(), Error> {
        let addr = "[::1]:50052";
        let example_server = GrpcServer::default().set_service_address(addr.to_string());
        let mut rt = Runtime::new().unwrap();
        rt.block_on(async {
            example_server.spawn_with_default_service().await?;
        });
    });
    */
    Ok(())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    spawn_servers()?;
    Ok(())
}
