//use futures::executor::block_on;
//use futures::executor::ThreadPool;
use sdr_grpc_lib::client;
use std::error::Error;
use tokio::runtime::Runtime;

fn just_get_result(sdr_addr: String) -> Result<Vec<u8>, Box<dyn Error>> {
    //let sdr_addr = "http://[::1]:50052";
    let client_config = client::ClientConfig::default().set_uri_from_string(sdr_addr);

    let rt = Runtime::new().unwrap();
    let file_response = client::Client::new()
        .set_config(client_config)
        .set_node_id("Ox002".to_string())
        .request_file("ReqID", "Ox001", "/home/alexander/test_file.txt");
    let resolved_future = rt.block_on(file_response);
    //let resolved_future = block_on(file_response);
    Ok(resolved_future?)
}

fn request_file(sdr_addr: String) -> Result<Vec<u8>, Box<dyn Error + Sync + Send>> {
    let pure_results = just_get_result(sdr_addr);
    //Ok(pure_results)
    match pure_results {
        Ok(r) => Ok(r),
        Err(_) => Err("whatever".into()),
    }
    //Err(e) => Err(Box::<dyn Error + Send + Sync>::from(e)),
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let resp = request_file("http://[::1]:50051".to_string());
    if resp.is_ok() {
        let byte_resp = resp.unwrap();
        println!("byte_resp.len(): {}", byte_resp.len());
    }
    Ok(())
}
