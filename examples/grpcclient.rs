use sdr_grpc_lib::client;
use std::fs;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let sdr_addr = "http://[::1]:50051";
    let client_config = client::ClientConfig::default().set_uri_from_string(sdr_addr.to_string());
    /*
      let res = client::Client::new()
          .set_config(client_config)
          .set_node_id("Ox002".to_string())
          .send_request("ReqID", "Ox001", "TestFile.txt")
          .await?;
    */
    let file_response = client::Client::new()
        .set_config(client_config)
        .set_node_id("Ox002".to_string())
        .request_file("ReqID", "Ox001", "TestFile.txt")
        .await?;

    let check_bytes = fs::read("TestFile.txt")?;

    assert_eq!(file_response, check_bytes);
    fs::write("results.txt", check_bytes)?;

    println!("Everything is fine!");

    Ok(())
}
