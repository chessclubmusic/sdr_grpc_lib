use sdr_grpc_lib::sdrinterface::send_receive_server::SendReceiveServer;
use sdr_grpc_lib::server::GrpcService;
use tonic::transport::Server;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let addr = "[::1]:50051".parse().unwrap();

    let grpc_service = GrpcService::default();

    Server::builder()
        .add_service(SendReceiveServer::new(grpc_service))
        .serve(addr)
        .await?;

    Ok(())
}
