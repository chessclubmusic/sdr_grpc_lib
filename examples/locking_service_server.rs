//use hyper::Client;
use hyper::Client;
use sdr_grpc_lib::server::{FileRequester, GetFileResults, GrpcServer};
//use sdr_grpc_lib::server::GrpcServer;
use log::info;
use std::error::Error;
//use tokio::runtime::Runtime;
use tokio::sync::mpsc;
use tokio::sync::oneshot;

struct ExampleRequester {
    file_id: String,
    file_server_addr: String,
}

impl ExampleRequester {
    pub fn new(file_name: String, server_addr: String) -> Self {
        ExampleRequester {
            file_id: file_name,
            file_server_addr: server_addr,
        }
    }
}

impl FileRequester for ExampleRequester {
    fn get_file(&self, _f_name: &str) -> oneshot::Receiver<GetFileResults> {
        let url_str = format!("{}/{}", self.file_server_addr, self.file_id);
        let (sender, receiver) = oneshot::channel::<GetFileResults>();
        println!("Sending request {}", url_str);
        tokio::spawn(async move {
            let url = url_str.parse::<hyper::Uri>().unwrap();
            let client = Client::new();
            let res = client.get(url).await;
            println!("client.get(url) returned result");
            match res {
                Ok(r) => {
                    let byte_res = hyper::body::to_bytes(r).await;
                    match byte_res {
                        Ok(br) => {
                            let output = br.to_vec();
                            info!("sending output {:?}", output);
                            sender.send(Ok(output))
                        }
                        Err(be) => sender.send(Err(Box::new(be))),
                    }
                }
                Err(e) => sender.send(Err(Box::new(e))),
            }
        });
        receiver
    }
}

async fn spawn_servers() -> Result<(), Box<dyn Error>> {
    let addrs = ["[::1]:50051"];
    //let addrs = ["[::1]:50051", "[::1]:50052"];

    let (tx, mut rx) = mpsc::unbounded_channel();
    for addr in &addrs {
        //let serve = GrpcServer::default()
        //std::thread::spawn(move )
        let tx = tx.clone();
        let requester = ExampleRequester::new(
            "test_file.txt".to_string(),
            "http://127.0.0.1:1337".to_string(),
        );
        //"index.html".to_string(),
        let res = GrpcServer::default()
            .set_service_address(addr.to_string())
            .spawn_locking_with_requester(Box::new(requester));

        tokio::spawn(async move {
            if let Err(e) = res.await {
                eprintln!("Error = {:?}", e);
            }

            tx.send(()).unwrap();
        });
    }

    rx.recv().await;
    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    //spawn_servers()?;
    spawn_servers().await?;
    println!("finished spawn_servers()");
    Ok(())
}
