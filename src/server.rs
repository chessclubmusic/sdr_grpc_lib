use crate::sdrinterface::send_receive_server::{SendReceive, SendReceiveServer};
use crate::sdrinterface::{Packet, SendFileRequest, SendSummary};
use futures::Stream;
use hyper::{Body, Client, Request as HyperRequest, Response as HyperResponse};
use log::{error, info};
use std::any::Any;
use std::error::Error;
use std::fs;
use std::pin::Pin;
use std::sync::{Arc, RwLock};
use std::task::{Context, Poll};
//use tokio::runtime::Runtime;
use tokio::sync::{mpsc, oneshot};
use tonic::{
    body::BoxBody,
    transport::{NamedService, Server},
    Request, Response, Status,
};
use tower::Service;

// struct for service
#[derive(Default)]
pub struct GrpcService {}

pub type GetFileResults = Result<Vec<u8>, Box<dyn Error + Sync + Send>>;

// implementing rpc for service defined in .proto
#[tonic::async_trait]
impl SendReceive for GrpcService {
    //type SendFileStream = mpsc::Receiver<Result<Packet, Status>>;
    type SendFileStream =
        Pin<Box<dyn Stream<Item = Result<Packet, Status>> + Send + Sync + 'static>>;

    async fn send(
        &self,
        request: Request<tonic::Streaming<Packet>>,
    ) -> Result<Response<SendSummary>, Status> {
        let n_packets: i32 = request.metadata().len() as i32;
        let mut pkts = request.into_inner();
        let mut pkt_vec: Vec<Packet> = Vec::new();
        while let Some(msg) = pkts.message().await? {
            pkt_vec.push(msg);
        }
        let n_pkts: i32 = pkt_vec.len() as i32;
        println!("n_pkts: {}", n_pkts);
        let send_summary = SendSummary {
            txpackets: n_packets,
        };
        Ok(Response::new(send_summary))
    }
    //out rpc implemented as function
    async fn send_file(
        &self,
        request: Request<SendFileRequest>,
    ) -> Result<Response<Self::SendFileStream>, Status> {
        let mut file_as_bytes = fs::read(request.get_ref().resourceid.clone())?;
        let max_data_size = 500;
        let mut n_chunks = (file_as_bytes.len() / max_data_size) as i32;
        let packet_remainder = file_as_bytes.len() % max_data_size;
        if packet_remainder > 0 {
            n_chunks = n_chunks + 1;
        }
        // create a queue/channel
        let (tx, rx) = mpsc::channel(n_chunks as usize);
        //let (mut tx, rx) = mpsc::channel(n_chunks as usize);
        // create a new task
        tokio::spawn(async move {
            for i in 0..n_chunks {
                let mut chunk_size = max_data_size;
                if max_data_size > file_as_bytes.len() {
                    chunk_size = file_as_bytes.len();
                }
                let cur_chunk: Vec<_> = file_as_bytes.drain(0..chunk_size).collect();
                let chunk_str = String::from_utf8(cur_chunk).unwrap();
                // send packets over our channel
                tx.send(Ok(Packet {
                    requestid: String::from(request.get_ref().clone().requestid),
                    vlanid: String::from("001"),
                    sourceid: String::from(request.get_ref().clone().sourceid),
                    destinationid: String::from(request.get_ref().clone().destinationid),
                    pktid: i + 1 as i32,
                    numtotal: n_chunks as i32,
                    data: chunk_str,
                }))
                .await
                .unwrap();
            }
        });
        // return response as SendSummary message as defined in .proto
        Ok(Response::new(Box::pin(
            tokio_stream::wrappers::ReceiverStream::new(rx),
        )))
    }
}

#[derive(Clone)]
pub struct LockingService {
    requester: Arc<RwLock<Box<dyn FileRequester>>>,
}

pub trait FileRequester: Any + Send + Sync {
    fn get_file(&self, f_name: &str) -> oneshot::Receiver<GetFileResults>;
}

#[derive(Default)]
pub struct NullFileRequester {}

impl NullFileRequester {
    pub fn new() -> NullFileRequester {
        NullFileRequester::default()
    }
}

impl FileRequester for NullFileRequester {
    fn get_file(&self, _f_name: &str) -> oneshot::Receiver<GetFileResults> {
        unimplemented!()
    }
}

impl LockingService {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn configure_requester(
        &self,
        requester: Box<dyn FileRequester>,
    ) -> Result<(), Box<dyn Error + Sync + Send>> {
        let mut lock = self.requester.write().unwrap();
        *lock = requester;

        Ok(())
    }
}

impl Default for LockingService {
    fn default() -> Self {
        LockingService {
            requester: Arc::new(RwLock::new(Box::new(NullFileRequester::new()))),
        }
    }
}

// implementing rpc for service defined in .proto
#[tonic::async_trait]
impl SendReceive for LockingService {
    //type SendFileStream = mpsc::Receiver<Result<Packet, Status>>;
    type SendFileStream =
        Pin<Box<dyn Stream<Item = Result<Packet, Status>> + Send + Sync + 'static>>;
    //out rpc implemented as function

    async fn send(
        &self,
        request: Request<tonic::Streaming<Packet>>,
    ) -> Result<Response<SendSummary>, Status> {
        let mut pkts = request.into_inner();
        let mut pkt_vec: Vec<Packet> = Vec::new();
        while let Some(msg) = pkts.message().await? {
            pkt_vec.push(msg);
        }
        let n_pkts: i32 = pkt_vec.len() as i32;
        println!("n_pkts: {}", n_pkts);
        let send_summary = SendSummary { txpackets: n_pkts };
        Ok(Response::new(send_summary))
    }

    async fn send_file(
        &self,
        request: Request<SendFileRequest>,
    ) -> Result<Response<Self::SendFileStream>, Status> {
        let get_file_receiver = self
            .requester
            .read()
            .unwrap()
            .get_file(&request.get_ref().resourceid.clone());
        let res = get_file_receiver.await;
        let mut file_as_bytes: Vec<u8>;
        match res {
            Ok(r) => {
                if r.is_err() {
                    error!(
                        "ERROR FILE {} NOT FOUND",
                        request.get_ref().resourceid.clone()
                    );
                    return Err(Status::not_found("File not found".to_string()));
                }
                let results_bytes = r.unwrap();
                file_as_bytes = results_bytes.clone();
                let _results_str = std::str::from_utf8(&results_bytes).unwrap();
                //info!("get_file_result: {}", results_str);
                //println!("get_file_result: {}", results_str);
            }
            Err(e) => {
                error!("get_file() produced an error: {:?}", e);
                return Err(Status::not_found(e.to_string()));
            }
        }
        //let mut file_as_bytes = res.unwrap().unwrap();
        //let mut file_as_bytes = fs::read(request.get_ref().resourceid.clone())?;
        let max_data_size = 500;
        let mut n_chunks = (file_as_bytes.len() / max_data_size) as i32;
        let packet_remainder = file_as_bytes.len() % max_data_size;
        if packet_remainder > 0 {
            n_chunks = n_chunks + 1;
        }
        // create a queue/channel
        //let (mut tx, rx) = mpsc::channel(n_chunks as usize);
        let (tx, rx) = mpsc::channel(n_chunks as usize);
        // create a new task
        tokio::spawn(async move {
            for i in 0..n_chunks {
                let mut chunk_size = max_data_size;
                if max_data_size > file_as_bytes.len() {
                    chunk_size = file_as_bytes.len();
                }
                let cur_chunk: Vec<_> = file_as_bytes.drain(0..chunk_size).collect();
                let chunk_str = String::from_utf8(cur_chunk).unwrap();
                // send packets over our channel
                tx.send(Ok(Packet {
                    requestid: String::from(request.get_ref().clone().requestid),
                    vlanid: String::from("001"),
                    sourceid: String::from(request.get_ref().clone().sourceid),
                    destinationid: String::from(request.get_ref().clone().destinationid),
                    pktid: i + 1 as i32,
                    numtotal: n_chunks as i32,
                    data: chunk_str,
                }))
                .await
                .unwrap();
            }
        });
        // return response as SendSummary message as defined in .proto
        Ok(Response::new(Box::pin(
            tokio_stream::wrappers::ReceiverStream::new(rx),
        )))
    }
}

#[derive(Clone, Debug)]
pub struct GrpcServer {
    max_packet_size: usize,
    service_addr: String,
    container_id: String,
}

impl Default for GrpcServer {
    fn default() -> Self {
        Self {
            max_packet_size: 500,
            service_addr: String::from("[::1]:50051"),
            container_id: String::from(""),
        }
    }
}

impl GrpcServer {
    pub fn set_service_address(mut self, addr: String) -> Self {
        self.service_addr = addr;
        self
    }

    pub fn set_container_id(mut self, container_id: String) -> Self {
        self.container_id = container_id;
        self
    }

    pub async fn spawn_with_default_service(self) -> Result<(), Box<dyn Error>> {
        let service = GrpcService::default();

        println!("Starting service on {}", self.service_addr);
        let _serve = Server::builder()
            .add_service(SendReceiveServer::new(service))
            .serve(self.service_addr.parse().unwrap());

        Ok(())
    }

    //pub async fn new_spawn_and_serve(self) -> Result<(), Box<dyn Error>> {
    pub async fn new_spawn_and_serve(self) -> Result<(), tonic::transport::Error> {
        let service = GrpcService::default();

        println!("Starting service on {}", self.service_addr);
        let serve = Server::builder()
            .add_service(SendReceiveServer::new(service))
            .serve(self.service_addr.parse().unwrap());
        serve.await
    }

    pub async fn spawn_locking_with_requester(
        self,
        requester: Box<dyn FileRequester>,
    ) -> Result<(), tonic::transport::Error> {
        let service = LockingService::default();
        service.configure_requester(requester).unwrap();

        info!("Starting service on {}", self.service_addr);
        let serve = Server::builder()
            .add_service(SendReceiveServer::new(service))
            .serve(self.service_addr.parse().unwrap());
        serve.await
    }
}

#[derive(Debug, Clone)]
struct InterceptedService<S> {
    inner: S,
}

impl<S> Service<HyperRequest<Body>> for InterceptedService<S>
where
    S: Service<HyperRequest<Body>, Response = HyperResponse<BoxBody>>
        + NamedService
        + Clone
        + Send
        + 'static,
    S::Future: Send + 'static,
{
    type Response = S::Response;
    type Error = S::Error;
    type Future = futures::future::BoxFuture<'static, Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.inner.poll_ready(cx)
    }

    fn call(&mut self, req: HyperRequest<Body>) -> Self::Future {
        let mut svc = self.inner.clone();
        //println!("svc: {}", self);

        Box::pin(async move {
            let mut http_req_clone = http::Request::new(req.body().clone());
            *http_req_clone.headers_mut() = req.headers().clone();
            let ton_req = Request::from_http(http_req_clone);
            //println!("ton_req: {:?}", ton_req);
            info!("req extensions: {:?}", *req.extensions());
            let req_msg = ton_req.get_ref();
            println!("Req msg: {:?}", req_msg);
            //let sf_req = ton_req.into_streaming_request();
            let url = "http://127.0.0.1:1337/index.html"
                .parse::<hyper::Uri>()
                .unwrap();
            let client = Client::new();
            let res = client.get(url).await;
            match res {
                Ok(r) => println!("Response: {}", r.status()),
                Err(e) => println!("Error getting response {:?}", e),
            }
            //println!("Response: {}", res.status());
            svc.call(req).await
        })
    }
}

impl<S: NamedService> NamedService for InterceptedService<S> {
    const NAME: &'static str = S::NAME;
}
