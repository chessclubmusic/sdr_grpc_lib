use crate::sdrinterface::send_receive_client::SendReceiveClient;
use crate::sdrinterface::{Packet, SendFileRequest, SendSummary};

use futures::stream;
use http::Uri;
use rmp_serde::Serializer as OtherSerializer;
use serde::ser::{Serialize, SerializeStruct, Serializer};
use std::collections::HashMap;
use std::error::Error;
use std::time::Duration;
//use tokio_stream as stream;
use tonic::codec::Streaming;
use tonic::transport::Channel;
//use tonic::IntoStreamingRequest;
use tonic::Request;

extern crate bytes;

#[derive(Clone, Debug)]
pub struct ClientConfig {
    connection_timeout: Option<Duration>,
    uri: Option<Uri>,
}

impl Default for ClientConfig {
    fn default() -> Self {
        Self {
            connection_timeout: None,
            uri: None,
        }
    }
}

impl ClientConfig {
    pub fn set_connnection_timeout(mut self, timeout: impl Into<Option<Duration>>) -> Self {
        if let Some(timeout) = timeout.into() {
            self.connection_timeout = Some(timeout);
        }
        self
    }

    pub fn set_uri_from_string(mut self, uri_str: String) -> Self {
        let parsed_uri = uri_str.parse::<Uri>().unwrap();
        self.uri = Some(parsed_uri);
        self
    }
}

// A client to make outgoing grpc requests
#[derive(Clone, Debug)]
pub struct FlatClient {
    uri: Uri,
    node_id: String,
}

// A client to make outgoing grpc requests
#[derive(Clone, Debug)]
pub struct Client {
    config: ClientConfig,
    node_id: Option<String>,
}

impl Default for Client {
    fn default() -> Self {
        Self {
            config: ClientConfig::default(),
            node_id: None,
        }
    }
}

impl Serialize for SendSummary {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut state = serializer.serialize_struct("SendSummary", 1)?;
        state.serialize_field("txpackets", &self.txpackets)?;
        state.end()
    }
}

impl Client {
    pub fn new() -> Client {
        Client::default()
    }

    pub fn set_node_id(mut self, node_id: String) -> Self {
        self.node_id = Some(node_id);
        self
    }

    pub fn set_config(mut self, config: ClientConfig) -> Self {
        self.config = config.clone();
        self
    }

    pub async fn send_request(
        self,
        req_id: &str,
        src_id: &str,
        res_id: &str,
    ) -> Result<Streaming<Packet>, Box<dyn Error>> {
        let uri_copy = self.config.uri.as_ref().unwrap().clone();
        let channel = Channel::builder(uri_copy).connect_lazy()?;
        std::println!("Channel connected");

        let mut client = SendReceiveClient::new(channel);

        let request = Request::new(SendFileRequest {
            requestid: String::from(req_id),
            sourceid: String::from(src_id),
            destinationid: self.node_id.clone().unwrap(),
            resourceid: String::from(res_id),
        });
        let mut response = client.send_file(request).await?.into_inner();
        let mut data_recvd = String::new();
        while let Some(msg) = response.message().await? {
            println!("pktid: {}", msg.pktid);
            data_recvd.push_str(&msg.data);
        }
        println!("Size of received string: {}", data_recvd.len());
        println!("All done");
        Ok(response)
    }

    pub async fn send_bytes_stream(
        self,
        max_data_size: i32,
        file_as_bytes: Vec<u8>,
        request_id: String,
        destination_id: String,
        vlan_id: String,
    ) -> Result<Vec<u8>, Box<dyn Error>> {
        let uri_copy = self.config.uri.as_ref().unwrap().clone();
        let channel = Channel::builder(uri_copy).connect_lazy()?;

        let mut client = SendReceiveClient::new(channel);
        let mut pkt_id = 1;
        let mut pkt_vec: Vec<Packet> = Vec::new();
        let file_chunks = file_as_bytes.chunks(max_data_size as usize);
        let num_chunks = file_chunks.len();
        for chunk in file_chunks {
            let chunk_str = String::from_utf8(chunk.to_vec()).unwrap();
            let pkt = Packet {
                requestid: request_id.clone(),
                vlanid: vlan_id.clone(),
                sourceid: self.node_id.clone().unwrap(),
                destinationid: destination_id.clone(),
                pktid: pkt_id,
                numtotal: num_chunks as i32,
                data: chunk_str,
            };
            pkt_vec.push(pkt);
            pkt_id = pkt_id + 1;
        }
        let send_req = Request::new(stream::iter(pkt_vec));
        let response = client.send(send_req).await?;
        let summary = response.into_inner();
        let mut buf: Vec<u8> = Vec::new();
        summary.serialize(&mut OtherSerializer::new(&mut buf).with_struct_map())?;
        Ok(buf)
    }

    pub async fn send_packet_stream(
        self,
        req_id: &str,
        src_id: &str,
        res_id: &str,
    ) -> Result<Vec<u8>, Box<dyn Error>> {
        let uri_copy = self.config.uri.as_ref().unwrap().clone();
        let channel = Channel::builder(uri_copy).connect_lazy()?;

        let mut client = SendReceiveClient::new(channel);

        let request = Request::new(SendFileRequest {
            requestid: String::from(req_id),
            sourceid: String::from(src_id),
            destinationid: self.node_id.clone().unwrap(),
            resourceid: String::from(res_id),
        });
        let mut str_response: tonic::Streaming<Packet> =
            client.send_file(request).await?.into_inner();
        let mut pkt_vec: Vec<Packet> = Vec::new();
        while let Some(msg) = str_response.message().await? {
            pkt_vec.push(msg);
        }
        let send_req = Request::new(stream::iter(pkt_vec));
        let client_send = client.send(send_req);
        let send_response = client_send.await?;
        //let send_response = client.send(send_req.into_streaming_request()).await?;
        //println!("Size of received string: {}", data_recvd.len());
        let summary_struct = send_response.into_inner();
        println!("summary_struct: {:?}", summary_struct);
        let mut buf: Vec<u8> = Vec::new();
        summary_struct.serialize(&mut OtherSerializer::new(&mut buf).with_struct_map())?;
        println!("Size of buf: {}", buf.len());
        Ok(buf)
    }

    pub async fn request_file(
        self,
        req_id: &str,
        src_id: &str,
        res_id: &str,
    ) -> Result<Vec<u8>, Box<dyn Error>> {
        let uri_copy = self.config.uri.as_ref().unwrap().clone();
        let channel = Channel::builder(uri_copy).connect_lazy()?;

        let mut client = SendReceiveClient::new(channel);

        let request = Request::new(SendFileRequest {
            requestid: String::from(req_id),
            sourceid: String::from(src_id),
            destinationid: self.node_id.clone().unwrap(),
            resourceid: String::from(res_id),
        });
        let mut response = client.send_file(request).await?.into_inner();
        let mut pkts_received = HashMap::new();
        while let Some(msg) = response.message().await? {
            if !pkts_received.contains_key(&msg.pktid) {
                pkts_received.insert(msg.pktid, msg.data.clone());
            }
        }
        let mut pkt_keys = pkts_received.keys().copied().collect::<Vec<_>>();
        pkt_keys.sort_by(|a, b| a.cmp(b));
        //println!("pkt_keys: {:?}", pkt_keys);
        let mut data_recvd = String::new();
        for pkt_id in pkt_keys.iter() {
            data_recvd.push_str(&pkts_received[pkt_id]);
        }
        //println!("Size of received string: {}", data_recvd.len());
        Ok(data_recvd.into_bytes())
    }
}

#[cfg(test)]
mod tests {
    use http::Uri;
    use tonic::transport::{Channel, Endpoint};
    #[test]
    fn uri_no_scheme() {
        let uri_str = "[::1]:50051";
        let uri = uri_str.parse::<Uri>().unwrap();

        let _endpoint = Endpoint::from(uri.clone());
        assert!(uri.scheme().is_none());
        assert_eq!(uri.port_u16(), Some(50051));
    }

    #[test]
    fn channel_builder_from_uri() {
        let uri_str = "[::1]:50051";
        let uri = uri_str.parse::<Uri>().unwrap();
        let chan = Channel::builder(uri.clone());

        let dup_uri_str = "[::1]:50051";
        let chan_from_static = Channel::from_static(dup_uri_str);
        assert_eq!(chan.uri().scheme(), chan_from_static.uri().scheme());
    }

    #[test]
    fn lazy_connect() {
        let uri_str = "[::1]:50051";
        let uri = uri_str.parse::<Uri>().unwrap();
        let chan = Channel::builder(uri.clone());

        let conn_resp = chan.connect_lazy();
        assert_eq!(conn_resp.is_err(), true);
    }

    #[tokio::test]
    async fn channel_connect() {
        let uri_str = "[::1]:50051";
        let uri = uri_str.parse::<Uri>().unwrap();
        let chan = Channel::builder(uri.clone());

        let conn_resp = chan.connect().await;
        assert_eq!(conn_resp.is_err(), true);
        let dup_uri_str = "http://[::1]:50051";
        let chan_from_static = Channel::from_static(dup_uri_str);
        let other_resp = chan_from_static.connect().await;
        assert_eq!(other_resp.is_err(), true);
    }
}
