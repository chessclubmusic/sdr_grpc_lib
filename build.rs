fn main() -> std::io::Result<()> {
    let path = std::env::current_dir()?;
    println!("build.rs CURRENT DIR: {}", path.display());
    let protofile_exists = std::fs::metadata("proto/sdrinterface.proto").is_ok();
    println!("protofile exists: {}", protofile_exists);
    let compile_result = tonic_build::compile_protos("proto/sdrinterface.proto");
    match compile_result {
        Ok(r) => println!("Successfully compiled proto: {:?}", r),
        Err(e) => println!("Error compiling protocol file: {:?}", e),
    }
    //tonic_build::compile_protos("proto/sdrinterface.proto").unwrap();
    //tonic_build::compile_protos("proto/sdr_interface.proto").unwrap();
    Ok(())
}
